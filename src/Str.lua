-- String utility module
local Str = {}

--- Checks whether a string starts with match
---@param str string the string to check for a match
---@param match string the match to check for
function Str.startsWith(str, match)
    return string.sub(str, 1, string.len(match)) == match
end

--- Removes whitespace from a string by creating and returning a new one
--- @return string newStr the new string
function Str.trim(str)
    return string.gsub(str, " ", "")
end

--- Creates a new string by concatenating the table's elements, separted by the specified seperator string
--- @param tbl table the table containing the strings to be joined
--- @param seperator string the seperator joining the strings
--- @return string newStr the concatenated string
function Str.join(tbl: table, seperator: string)
    local str = ""

    for _, value in pairs(tbl) do
        if str ~= "" then
            str = str.format("%s%s%s", str, seperator, value)
        else
            str = value
        end
    end

    return str
end

return Str