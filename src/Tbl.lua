local Tbl = {}

--- The find() method returns the first element that passes the test
---@param tbl table the original table
---@param callback function a test function that returns a boolean, indicating whether or not the element passes the test
---@return table table the first element that passes the test
function Tbl.find(tbl: table, callback)
    for index, value in ipairs(tbl) do
        local passedTest = callback(value, index, tbl)

        if type(passedTest) ~= "boolean" then
            error("callback does not return a boolean")

        elseif passedTest == true then
            return value
        end
    end

    return nil
end

--- The findAll() method returns  all the element that passes the test
---@param tbl table the original table
---@param callback function a test function that returns a boolean, indicating whether or not the element passes the test
---@return table table the elements that passed the test
function Tbl.findAll(tbl: table, callback)
    local newTbl = {}

    for index, value in ipairs(tbl) do

        local passedTest = callback(value, index, tbl)

        if type(passedTest) ~= "boolean" then
            error("callback does not return a boolean")

        elseif passedTest == true then
            table.insert(newTbl, value)
        end
    end

    return newTbl
end

return Tbl