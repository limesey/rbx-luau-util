return function ()
    local Tbl = require(script.Parent.Parent.Tbl)

    describe("find", function()
        it("should return the first element that passes the test function", function()
            local originalTable = {1, 2, 3, 4, 5}

            local element = Tbl.find(originalTable, function(element, index, tbl)
                return element > 3
            end)

            expect(element).to.equal(4)
        end)
    end)

    describe("findAll", function()
        it("should return all elements that pass the test function", function()
            local originalTable = {1, 2, 4, 5}

            local elements = Tbl.findAll(originalTable, function(element, index, tbl)
                return element > 3
            end)

            expect(table.concat(elements)).to.equal(table.concat({4,5}))
        end)
    end)
end