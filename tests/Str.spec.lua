return function()
    local Str = require(script.Parent.Parent.Str)

    describe("startsWith", function()
        it("should return a true if the provided string starts with match", function()
            expect(Str.startsWith("Hello world", "Hello")).to.equal(true)
        end)

        it("should return a false if the provided string does not start with match", function()
            expect(Str.startsWith("Hello world", "Hi")).to.equal(false)
        end)
    end)

    describe("trim", function()
        it("should remove whitespace from the provided string", function()
            local trimmedString = Str.trim("Hello World")

            expect(trimmedString).to.equal("HelloWorld")
        end)
    end)
end